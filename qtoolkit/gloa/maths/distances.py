# ======================================================================
# Copyright CERFACS (November 2018)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy

import qtoolkit.core.data_structures.quantum_circuit as qcirc
import qtoolkit.core.utils.types as qtypes


def gloa_objective_function(
    gate_sequence: qcirc.QuantumCircuit,
    U: qtypes.UnitaryMatrix,
    correctness_weight: float,
    circuit_cost_weight: float,
    circuit_cost_func: qcirc.CircuitCostFunction,
) -> float:
    """Compute a modified GLOA objective function.

    The GLOA article is: https://arxiv.org/abs/1004.2242.

    This objective function has been modified because the one presented in the
    GLOA article may make the algorithm converge to a matrix :math:`M` such that
    :math:`U  M^\\dagger = - I_n`. This new objective function prevent this
    problem.

    :param gate_sequence: the sequence of quantum gate that is candidate to
        approximate the objective_unitary matrix.
    :param U: the unitary matrix we are searching an approximation for.
    :param correctness_weight: importance of the correctness of the circuit
        in the objective function. Corresponds to the parameter alpha in the
        original paper.
    :param circuit_cost_weight: importance of the circuit cost in the
        objective function. Corresponds to the parameter beta in the original
        paper.
    :param circuit_cost_func: a function that will associate a cost to a given
        sequence of quantum gates.
    :return: the fidelity of the approximation.
    """
    N = U.shape[0]
    UUT = U @ gate_sequence.matrix.T.conj()
    trace_fidelity = (1 + numpy.trace(UUT) / N) / 2
    correctness = correctness_weight * trace_fidelity
    circuit_cost = circuit_cost_weight / circuit_cost_func(gate_sequence)
    return numpy.abs(1 - (correctness + circuit_cost))
