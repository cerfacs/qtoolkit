qtoolkit: A Python 3 library for quantum algorithms
===================================================
The qtoolkit library aims at implementing various algorithms and data-structures
related to quantum computing.

User Documentation
------------------

..
    Commented.
    .. toctree::
        :maxdepth: 2

        install
        tutorial
        circuits
        gates
        simulation
        schedules
        development


Progress reports
----------------

.. toctree::
    :maxdepth: 2

    pages/reports


API Reference
-------------

.. toctree::
    :maxdepth: 2

    api/modules


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
