..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

|1st| of October to |1st| of December 2018
==========================================

Contextual setting
------------------

This document is a progress report. It summarises the work I have done during
the first 2 months of my 1-year contract at CERFACS.

Before the 1-year contract
~~~~~~~~~~~~~~~~~~~~~~~~~~

Before this 1-year contract, I have been hired as intern at CERFACS during 6
months and worked on quantum computing. The main goal of the internship was to
build a extensive bibliography of articles about quantum computing and its
applications in the field of scientific computing. See [Suau18]_.

Conclusions of the internship
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The bibliographic research and the implementations performed during this 6-month
internship led to the conclusion that one of the most promising quantum algorithm for
scientific computing was the HHL algorithm (see [HHL09]_ [HHLprimer18]_). The researches
also isolated several points of the algorithm that are not trivial to implement
efficiently:

#. The algorithm needs to implement as a sequence of quantum gates the unitary
   :math:`e^{iHt}` for :math:`H` an arbitrary Hermitian matrix and
   :math:`t \in \mathbb{R}_+^*`. Creating a sequence of quantum gates approximating the
   unitary transformation :math:`\vert x \rangle \mapsto e^{iHt}\vert x \rangle` is
   known as `Hamiltonian simulation`.

   The problem of Hamiltonian simulation is also central in other applications such as
   chemistry, in particular in ground-state computations.

#. Once the |QPE| algorithm has been successfully applied, the eigenvalues of the
   Hamiltonian matrix :math:`H` are encoded in a quantum register. The following step is
   then to invert these eigenvalues. The computation of the eigenvalues' multiplicative
   inverse has not been covered in the different read articles.

Based on these observations and on the common interests of several associates, CERFACS
has decided to start working on the implementation of Hamiltonian simulation algorithms.


Hamiltonian simulation
----------------------

As explained in the previous part, Hamiltonian simulation algorithms aims at
approximating the unitary operator :math:`e^{iHt}` with a restricted set of allowed
quantum gates.

There are several research papers about Hamiltonian simulation, below is a listing of
the most interesting ones:

#. |Simulating Hamiltonian dynamics on a small quantum computer|_. This is a
   presentation made by Andrew Childs on the methods that can be used to perform
   Hamiltonian simulation. In this presentation, 3 methods are outlined:

   #. A method based on product formulas. This method is a kind of divide-and-conquer
      approach.
   #. A method based on quantum walks.
   #. A method based on fractional queries.

   A summary of the different method complexities and capabilities is copied below.

   +---------------------------+------------------+---------------+--------------------+
   |                           | Product formulas | Quantum walks | Fractional queries |
   +===========================+==================+===============+====================+
   | Query complexity          | |compl_prodform| | |compl_qwalk| | |compl_frac_query| |
   +---------------------------+------------------+---------------+--------------------+
   | Best known scaling with   | ×                | ✓             | ×                  |
   | evolution time :math:`t`  |                  |               |                    |
   | and sparsity :math:`d`    |                  |               |                    |
   +---------------------------+------------------+---------------+--------------------+
   | Best known scaling with   | ×                | ×             | ✓                  |
   | error :math:`\epsilon`    |                  |               |                    |
   +---------------------------+------------------+---------------+--------------------+
   | Handles time-dependent    | ✓                | ×             | ✓                  |
   | Hamiltonians              |                  |               |                    |
   +---------------------------+------------------+---------------+--------------------+



.. |Simulating Hamiltonian dynamics on a small quantum computer| replace:: \
   Simulating Hamiltonian dynamics on a :sub:`small` quantum computer
.. _Simulating Hamiltonian dynamics on a small quantum computer: \
   https://www.cs.umd.edu/~amchilds/talks/ibm13.pdf

.. |compl_prodform| replace:: :math:`d^3 \vert\vert H\vert\vert t \left(
   \frac{d \vert\vert H\vert\vert t}{\epsilon} \right)^{\mathcal{o}(1)}`
.. |compl_qwalk| replace:: :math:`\mathcal{0}\left( \frac{d \vert\vert H\vert\vert_{max}
   t}{\sqrt{\epsilon}} \right)`
.. |compl_frac_query| replace:: :math:`\mathcal{O}\left( \tau \frac{\log \left( \tau /
   \epsilon\right)}{\log \log \left( \tau / \epsilon\right)} \right) \\ \tau := d^2
   \vert\vert H\vert\vert_{max} t`
.. |br| raw:: html

   <br />


The Solovay-Kitaev algorithm
----------------------------

References
----------

.. |1st| replace:: 1\ :sup:`st`
.. |QPE| replace:: Quantum Phase Estimation

.. [Suau18] https://cerfacs.fr/wp-content/uploads/2018/09/CSG_Suau-final_report.pdf
.. [HHL09] https://arxiv.org/abs/0811.3171
.. [HHLprimer18] https://arxiv.org/abs/1802.08227
.. [Simulating Hamiltonian dynamics on a small quantum computer] https://www.cs.umd.edu/~amchilds/talks/ibm13.pdf
